INSERT INTO Presupuesto(id,concepto,tipo,valor) values(1,'Recursos Humanos', 'I', 6000000);
INSERT INTO Presupuesto(id,concepto,tipo,valor) values(2,'Herramientas', 'I', 1500000);
INSERT INTO Presupuesto(id,concepto,tipo,valor) values(3,'Servicios Externos', 'I', 3500000);
INSERT INTO Presupuesto(id,concepto,tipo,valor) values(4,'Recursos Humanos', 'A', 1000000);
INSERT INTO Presupuesto(id,concepto,tipo,valor) values(5,'Herramientas', 'A', 400000);
INSERT INTO Presupuesto(id,concepto,tipo,valor) values(6,'Servicios Externos', 'A', 700000);

INSERT INTO Concepto(id,concepto) values(1,'Recursos Humanos');
INSERT INTO Concepto(id,concepto) values(2,'Herramientas');
INSERT INTO Concepto(id,concepto) values(3,'Servicios Externos');

INSERT INTO Tipo(id,tipo,concepto) values(1,'I','Presupuesto Inicial');
INSERT INTO Tipo(id,tipo,concepto) values(2,'A','Presupuesto Actualizado');

INSERT INTO Proyecto(id,codigo,nombre,inicio,termino) values(1,'TE_3120','Curso Spring Fundamentals','2019-01-05','2019-11-17');

INSERT INTO Etapa(ID,PROYECTO_ID,NOMBRE,ETAPA,INICIO,TERMINO,ORDEN) VALUES(1,1,'Bob','Código','05-03-2019','05-04-2019',6);
INSERT INTO Etapa(ID,PROYECTO_ID,NOMBRE,ETAPA,INICIO,TERMINO,ORDEN) values(3,1,'Bob','QA','03-03-2019','09-03-2019',8);
INSERT INTO Etapa(ID,PROYECTO_ID,NOMBRE,ETAPA,INICIO,TERMINO,ORDEN) values(5,1,'Bob','Validación','11-03-2019','16-03-2019',10);
INSERT INTO Etapa(ID,PROYECTO_ID,NOMBRE,ETAPA,INICIO,TERMINO,ORDEN) values(6,1,'Bob','Diseño','01-03-2019','03-03-2019',11);

INSERT INTO Etapa(ID,PROYECTO_ID,NOMBRE,ETAPA,INICIO,TERMINO,ORDEN) values(11,1,'Joe','Entrega','20-03-2019','22-03-2019',5);

INSERT INTO Etapa(ID,PROYECTO_ID,NOMBRE,ETAPA,INICIO,TERMINO,ORDEN) values(16,1,'Dan','Entrega','20-03-2019','22-03-2019',16);

INSERT INTO Responsable(NOMBRE) values('Cristian');
INSERT INTO Responsable(NOMBRE) values('Gonzalo');
INSERT INTO Responsable(NOMBRE) values('Marité');
INSERT INTO Responsable(NOMBRE) values('Karina');

INSERT INTO Actividad(PROYECTO_ID,ORDEN,NOMBRE,ETAPA_ID,INICIO,TERMINO,RESPONSABLE_ID) values(1,1,'API Planificación',1,'05-03-2019','15-03-2019',1);

INSERT INTO Cumplimiento(progress_plan, progress_real, progress_deviation, init_plan, init_real, init_deviation, end_plan, end_real, end_deviation, responsable)
values(16, 10, 4, 40, 30, 20, 50, 30, 20, 'Gonzalo');

