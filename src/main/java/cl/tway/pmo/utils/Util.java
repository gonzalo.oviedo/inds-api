package cl.tway.pmo.utils;

public class Util {

	public static String revertEnglishDateToChileanDate(String date) {
		StringBuffer chileanDate = new StringBuffer();
		String[] arrOfStr = date.split("-");

		chileanDate.append(arrOfStr[2]);
		chileanDate.append("-");
		chileanDate.append(arrOfStr[1]);
		chileanDate.append("-");
		chileanDate.append(arrOfStr[0]);

		return chileanDate.toString();

	}

}
