package cl.tway.pmo.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Fecha {
	/**
	 * Fecha formato ejemplo 23/09/2007 
	 * @param fecha
	 * @return
	 * @throws ParseException
	 */
	public static Long getTimeStamp(String fecha) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = dateFormat.parse(fecha);
		long time = date.getTime();
		long ts = new Timestamp(time).getTime();
		return ts;
	}
	
	public static Long getTimeStampEnglish(String fecha) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = dateFormat.parse(fecha);
		long time = date.getTime();
		long ts = new Timestamp(time).getTime();
		return ts;
	}
}
