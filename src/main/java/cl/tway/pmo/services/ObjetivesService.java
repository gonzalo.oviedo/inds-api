package cl.tway.pmo.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class ObjetivesService {

	public List<Map<String, Object>> getChartSummary() {
		List<Map<String, Object>> l = new ArrayList<Map<String, Object>>();
		
		List<Integer> n = null;
		Map<String, Object> d = null;
		
		n = new ArrayList<Integer>();
		n.add(20);
		n.add(0);
		n.add(0);
		d = new HashMap<String, Object>();
		d.put("name", "Objetivo 1");
		d.put("data", n);
		l.add(d);
		
		n = new ArrayList<Integer>();
		n.add(0);
		n.add(33);
		n.add(0);
		d = new HashMap<String, Object>();
		d.put("name", "Objetivo 2");
		d.put("data", n);
		l.add(d);
		
		n = new ArrayList<Integer>();
		n.add(0);
		n.add(0);
		n.add(50);
		d = new HashMap<String, Object>();
		d.put("name", "Objetivo 3");
		d.put("data", n);
		l.add(d);
		
		return l;	
	}

	public Map<String, List<Integer>> getCircleData() {
		Map<String, List<Integer>> a = new HashMap<String, List<Integer>>();
		
		List<Integer> n = new  ArrayList<Integer>();
		
		n.add(76);
		n.add(100);
		n.add(61);
		n.add(40);
		
		a.put("series", n);
		
		return a;
	}
}
