package cl.tway.pmo.services;


import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.tway.pmo.repositories.documents.Data;
import cl.tway.pmo.repositories.documents.NombresChart;
import cl.tway.pmo.repositories.documents.budget.PieTwo;
import cl.tway.pmo.repositories.documents.charts.DataSeriesTypeOneDTO;
import cl.tway.pmo.repositories.documents.charts.DataSeriesTypeThreeDTO;
import cl.tway.pmo.repositories.documents.charts.DataSeriesTypeTwoDTO;
import cl.tway.pmo.repositories.documents.charts.helpers.Goal;
import cl.tway.pmo.repositories.entities.h2.Concepto;
import cl.tway.pmo.repositories.entities.h2.Etapa;
import cl.tway.pmo.repositories.entities.h2.Presupuesto;
import cl.tway.pmo.repositories.entities.h2.Tipo;
import cl.tway.pmo.repositories.interfaces.BudgetRepository;
import cl.tway.pmo.repositories.interfaces.ConceptRepository;
import cl.tway.pmo.repositories.interfaces.EtapaRepository;
import cl.tway.pmo.repositories.interfaces.TypeRepository;
import cl.tway.pmo.utils.Fecha;

@Service
public class BudgetService {
	
	@Autowired
	BudgetRepository br;
	@Autowired
	ConceptRepository cr;
	@Autowired
	TypeRepository tr;
	@Autowired
	EtapaRepository er;

	public Integer getSemiCirularUpdatedBudget() {
		// Obtiene el presupuesto actualizado
		Integer total = 0;
				
		total = br.findAll().stream().filter(presupuesto -> presupuesto.getTipo().equals("A")).map(presupuesto -> presupuesto.getValor()).reduce(0, Integer::sum);
		
		return total;
	}
	
	public List<Presupuesto> getAll() {
		
		List<Presupuesto> presupuestos = br.findAll();
		
		return br.findAll();
	}

	public Map<String, Integer> getInitialAndBalance() {
		
		Integer initial, balance = 0;
		
		initial= br.findAll().stream().filter(presupuesto -> presupuesto.getTipo().equals("I")).map(presupuesto -> presupuesto.getValor()).reduce(0, Integer::sum);
		balance = br.findAll().stream().filter(presupuesto -> presupuesto.getTipo().equals("A")).map(presupuesto -> presupuesto.getValor()).reduce(0, Integer::sum);
		
		Integer balancePercentage = balance*100/initial;
		
		Map<String, Integer> budget = new HashMap<String, Integer>();
		
		budget.put("initial", initial);
		budget.put("balance", balance);
		budget.put("balancePercentage", balancePercentage);
		
		return budget;
	}

	public List<?> getTypes() {
		return tr.findAll();
	}

	public List<?> getConcepts() {
		return cr.findAll();
	}

	public void saveExpense(Map<String, String> expenses) {
		Integer idType = Integer.parseInt(expenses.get("type").toString());
		Integer idConcept = Integer.parseInt(expenses.get("concept").toString());
		Integer expense = Integer.parseInt(expenses.get("expense").toString());
		
		Optional<Tipo> t = tr.findById(idType);
		Optional<Concepto> c = cr.findById(idConcept);
		
		Presupuesto p = new Presupuesto();
		
		if(t.isPresent() && c.isPresent()) {
			p.setConcepto(c.get().getConcepto());
			p.setTipo(t.get().getTipo());
			p.setValor(expense);
			
			br.saveAndFlush(p);
		}
	}

	public PieTwo getPieTwo() {
		
		PieTwo pt = new PieTwo();
			
		List<String> labels = new ArrayList<String>();
		List<Integer> datasets = new ArrayList<Integer>();
		
		Iterator<Presupuesto> pad = br.findAll().stream().filter(presupuesto -> presupuesto.getTipo().equals("A")).iterator();
		
		Map<String, Integer> sumaConceptos = new HashMap<String, Integer>();
		
		List<Presupuesto> pa = new ArrayList<Presupuesto>();
		pad.forEachRemaining(pa::add);
		
		for(Presupuesto p:pa) {
			String concepto = p.getConcepto();
			
			Optional<Integer> valorAnterior = Optional.ofNullable(sumaConceptos.get(concepto));
			
			if(valorAnterior.isPresent()) {
				Integer va = valorAnterior.get();
				sumaConceptos.put(concepto, p.getValor()+va);
			} else {
				sumaConceptos.put(concepto, p.getValor());
			}	
		} 
		
		// Convetimos en labels y en datasets
		for (Map.Entry<String, Integer> entry : sumaConceptos.entrySet()) {
			labels.add(entry.getKey());
		    datasets.add(entry.getValue());
		}
		
		pt.setLabels(labels);
		pt.setDatasets(datasets);
		
		return pt;
	}
	
	public List<NombresChart> timeLineChart() throws ParseException {
		
		List<NombresChart> chart = new ArrayList<NombresChart>();
		
		ArrayList<Long> fechas = new ArrayList<Long>();
		Data dto = null;
		
		// Bob
		List<Data> listaDataBob = new ArrayList<Data>();
		List<Etapa> bobData = new ArrayList<Etapa>();
		er.findAll().stream().filter(d -> d.getNombre().equals("Bob")).iterator().forEachRemaining(bobData::add);
		for(Etapa etapa: bobData) {
			fechas = new ArrayList<Long>();
			fechas.add(Fecha.getTimeStampEnglish(etapa.getInicio()));
			fechas.add(Fecha.getTimeStampEnglish(etapa.getTermino()));
			dto = new Data(etapa.getEtapa(), fechas);
			listaDataBob.add(dto);
		}
		NombresChart bob = new NombresChart("Bob", listaDataBob);
		
		// Joe
		List<Data> listaDataJoe = new ArrayList<Data>();
		List<Etapa> joeData = new ArrayList<Etapa>();
		er.findAll().stream().filter(d -> d.getNombre().equals("Joe")).iterator().forEachRemaining(joeData::add);
		for(Etapa etapa: joeData) {
			fechas = new ArrayList<Long>();
			fechas.add(Fecha.getTimeStampEnglish(etapa.getInicio()));
			fechas.add(Fecha.getTimeStampEnglish(etapa.getTermino()));
			dto = new Data(etapa.getEtapa(), fechas);
			listaDataJoe.add(dto);
		}
		NombresChart joe = new NombresChart("Joe", listaDataJoe);
		
		// Dan
		List<Data> listaDataDan = new ArrayList<Data>();
		List<Etapa> danData = new ArrayList<Etapa>();
		er.findAll().stream().filter(d -> d.getNombre().equals("Dan")).iterator().forEachRemaining(danData::add);
		for(Etapa etapa: danData) {
			fechas = new ArrayList<Long>();
			fechas.add(Fecha.getTimeStampEnglish(etapa.getInicio()));
			fechas.add(Fecha.getTimeStampEnglish(etapa.getTermino()));
			dto = new Data(etapa.getEtapa(), fechas);
			listaDataDan.add(dto);
		}
		NombresChart dan = new NombresChart("Dan", listaDataDan);
		
		chart.add(bob);
		chart.add(joe);
		chart.add(dan);
		
		return chart;
	}
	
	public List<Etapa> stages() throws ParseException {
		return er.findAll();
	}
	
	/**
	 * Data en duro
	 * @return
	 * @throws ParseException
	 */
	public List<NombresChart> timeLineChart2() throws ParseException {
		
		List<NombresChart> chart = new ArrayList<NombresChart>();
		
		ArrayList<Long> fechas = new ArrayList<Long>();
		Data dto = null;
		
		// Bob
		List<Data> listaDataBob = new ArrayList<Data>();
		 
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-05"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-08"));
		dto = new Data("Diseño", fechas);
		listaDataBob.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-02"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-05"));
		dto = new Data("Código", fechas);
		listaDataBob.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-05"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-07"));
		dto = new Data("Código", fechas);
		listaDataBob.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-03"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-09"));
		dto = new Data("QA", fechas);
		listaDataBob.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-08"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-11"));
		dto = new Data("QA", fechas);
		listaDataBob.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-11"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-16"));
		dto = new Data("Validación", fechas);
		listaDataBob.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-01"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-03"));
		dto = new Data("Diseño", fechas);
		listaDataBob.add(dto);
				
		NombresChart bob = new NombresChart("Bob", listaDataBob);
		
		
		// Joe
		List<Data> listaDataJoe = new ArrayList<Data>();
		 
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-10"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-16"));
		dto = new Data("Diseño", fechas);
		listaDataJoe.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-02"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-05"));
		dto = new Data("Diseño", fechas);
		listaDataJoe.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-06"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-16"));
		dto = new Data("QA", fechas);
		listaDataJoe.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-03"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-07"));
		dto = new Data("Código", fechas);
		listaDataJoe.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-20"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-22"));
		dto = new Data("Entrega", fechas);
		listaDataJoe.add(dto);
				
		NombresChart joe = new NombresChart("Joe", listaDataJoe);
		
		// Dan
		List<Data> listaDataDan = new ArrayList<Data>();
		 
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-10"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-17"));
		dto = new Data("Código", fechas);
		listaDataDan.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-05"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-09"));
		dto = new Data("Validación", fechas);
		listaDataDan.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-06"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-16"));
		dto = new Data("QA", fechas);
		listaDataDan.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-03"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-07"));
		dto = new Data("Código", fechas);
		listaDataDan.add(dto);
		
		fechas = new ArrayList<Long>();
		fechas.add(Fecha.getTimeStampEnglish("2019-03-20"));
		fechas.add(Fecha.getTimeStampEnglish("2019-03-22"));
		dto = new Data("Entrega", fechas);
		listaDataDan.add(dto);
				
		NombresChart dan = new NombresChart("Dan", listaDataDan);
		
		
		chart.add(bob);
		chart.add(joe);
		chart.add(dan);
		
		return chart;
	}

	public List<String> stagesInExistence() {
		
		List<String> etapas = new ArrayList<String>();
		er.findAll().stream().map(Etapa::getEtapa).distinct().iterator().forEachRemaining(etapas::add);
		
		return etapas;
	}

	public List<DataSeriesTypeOneDTO> chartPeriodTrack() {
		List<DataSeriesTypeOneDTO> series = new ArrayList<DataSeriesTypeOneDTO>();
		
		List<Integer> data = null;
		DataSeriesTypeOneDTO d = null;
		
		d = new DataSeriesTypeOneDTO();
		d.setName("Presupuesto");
		d.setType("column");
		data = new ArrayList<Integer>();
		data.add(211);
		data.add(203);
		data.add(331);
		data.add(340);
		data.add(441);
		data.add(449);
		data.add(565);
		data.add(585);
		d.setData(data);
		
		series.add(d);
		
		d = new DataSeriesTypeOneDTO();
		d.setName("Gasto");
		d.setType("column");
		data = new ArrayList<Integer>();
		data.add(100);
		data.add(190);
		data.add(207);
		data.add(260);
		data.add(304);
		data.add(350);
		data.add(400);
		data.add(380);
		d.setData(data);

		series.add(d);
		
		d = new DataSeriesTypeOneDTO();
		d.setName("Gasto/Presupuesto %");
		d.setType("line");
		data = new ArrayList<Integer>();
		data.add(47);
		data.add(94);
		data.add(63);
		data.add(69);
		data.add(78);
		data.add(80);
		data.add(71);
		data.add(65);
		d.setData(data);

		series.add(d);
		
		return series;
	}

	public List<DataSeriesTypeTwoDTO> chartTrackByCostCenter() {
		
		
		List<DataSeriesTypeTwoDTO> lista = new ArrayList<DataSeriesTypeTwoDTO>();
		Goal goal = null;
		ArrayList<Goal> goals = null;
		cl.tway.pmo.repositories.documents.charts.helpers.Data data = null;
		
		ArrayList<cl.tway.pmo.repositories.documents.charts.helpers.Data> lotData = new ArrayList<cl.tway.pmo.repositories.documents.charts.helpers.Data>();
		
		// Recursos Humanos
		goals = new ArrayList<Goal>();
		goal = new Goal("Presupuesto", 140, (short)2, (short)2, "#775DD0");
		goals.add(goal);
		data = new cl.tway.pmo.repositories.documents.charts.helpers.Data("Recursos Humanos", 120, goals);
		
		lotData.add(data);
		
		// Tecnologia
		
		goals = new ArrayList<Goal>();
		goal = new Goal("Presupuesto", 540, (short)5, (short)10, "#775DD0");
		goals.add(goal);
		data = new cl.tway.pmo.repositories.documents.charts.helpers.Data("Tecnología", 440, goals);
		
		lotData.add(data);
		
		// Administracion
		
		goals = new ArrayList<Goal>();
		goal = new Goal("Presupuesto", 520, (short)10, (short)0, "roud", "#775DD0");
		goals.add(goal);
		data = new cl.tway.pmo.repositories.documents.charts.helpers.Data("Administración", 540, goals);
		
		lotData.add(data);
		
		// Operaciones
		
		goals = new ArrayList<Goal>();
		goal = new Goal("Presupuesto", 610, (short)10, (short)0, "round", "#775DD0");
		goals.add(goal);
		data = new cl.tway.pmo.repositories.documents.charts.helpers.Data("Operaciones", 660, goals);
		
		lotData.add(data);
		
		// Comercial
		
		goals = new ArrayList<Goal>();
		goal = new Goal("Presupuesto", 660, (short)10, (short)0, "round", "#775DD0");
		goals.add(goal);
		data = new cl.tway.pmo.repositories.documents.charts.helpers.Data("Comercial", 810, goals);
		
		lotData.add(data);
		
		DataSeriesTypeTwoDTO d = new DataSeriesTypeTwoDTO("Areas",lotData);
		
		lista.add(d);
		return lista;
	}

	public List<DataSeriesTypeThreeDTO> chartSummaryBudget() {
		
		List<DataSeriesTypeThreeDTO> series = new ArrayList<DataSeriesTypeThreeDTO>();
		
		DataSeriesTypeThreeDTO  d = null;
		List<Integer> data  = null;
		
		data = new ArrayList<Integer>();
		data.add(3125);
		data.add(0);
		data.add(0);
		d = new DataSeriesTypeThreeDTO();
		d.setName("Monto Total");
		d.setData(data);
		
		series.add(d);
		
		data = new ArrayList<Integer>();
		data.add(0);
		data.add(2191);
		data.add(0);
		d = new DataSeriesTypeThreeDTO();
		d.setName("Gasto Real");
		d.setData(data);
		
		series.add(d);
		
		data = new ArrayList<Integer>();
		data.add(0);
		data.add(0);
		data.add(2000);
		d = new DataSeriesTypeThreeDTO();
		d.setName("Gasto Planificado");
		d.setData(data);
		
		series.add(d);
		
		data = new ArrayList<Integer>();
		data.add(0);
		data.add(934);
		data.add(0);
		d = new DataSeriesTypeThreeDTO();
		d.setName("Saldo");
		d.setData(data);

		series.add(d);
		
		return series;
	}

	public List<DataSeriesTypeThreeDTO> chartCostCenter() {
List<DataSeriesTypeThreeDTO> series = new ArrayList<DataSeriesTypeThreeDTO>();
		
		DataSeriesTypeThreeDTO  d = null;
		List<Integer> data  = null;
		
		data = new ArrayList<Integer>();
		data.add(440);
		data.add(550);
		data.add(410);
		data.add(370);
		data.add(220);
		data.add(403);
		data.add(201);
		d = new DataSeriesTypeThreeDTO();
		d.setName("Recursos Humanos");
		d.setData(data);
		
		series.add(d);
		

		data = new ArrayList<Integer>();
		data.add(503);
		data.add(320);
		data.add(330);
		data.add(50);
		data.add(130);
		data.add(430);
		data.add(320);
		d = new DataSeriesTypeThreeDTO();
		d.setName("Tecnología");
		d.setData(data);
		
		series.add(d);
		
		data = new ArrayList<Integer>();
		data.add(120);
		data.add(170);
		data.add(110);
		data.add(90);
		data.add(150);
		data.add(110);
		data.add(200);
		d = new DataSeriesTypeThreeDTO();
		d.setName("Administración y Finanzas");
		d.setData(data);
		
		series.add(d);
		
		data = new ArrayList<Integer>();
		data.add(90);
		data.add(70);
		data.add(50);
		data.add(80);
		data.add(60);
		data.add(90);
		data.add(40);
		d = new DataSeriesTypeThreeDTO();
		d.setName("Operaciones");
		d.setData(data);
		
		series.add(d);
		
		data = new ArrayList<Integer>();
		data.add(250);
		data.add(120);
		data.add(190);
		data.add(320);
		data.add(250);
		data.add(240);
		data.add(100);
		d = new DataSeriesTypeThreeDTO();
		d.setName("Comercial");
		d.setData(data);
		
		series.add(d);

		return series;
	}
}
