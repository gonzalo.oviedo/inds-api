package cl.tway.pmo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.tway.pmo.exceptions.PmoNotFoundException;
import cl.tway.pmo.repositories.documents.KpiDTO;
import cl.tway.pmo.repositories.interfaces.KpiRepository;

@Service
public class KpiService {

	@Autowired
	KpiRepository kpi;

	public KpiDTO getKpi(String tag) {

		Optional<List<KpiDTO>> of = Optional.of(kpi.findAll());
		KpiDTO kpi = null;
	
		if(of.isPresent()) {
			for(KpiDTO k:of.get()) {
				if(k.getTag().equals(tag)) {
					kpi = k;
				}
			}
			
			if(kpi==null) {
				throw new PmoNotFoundException("No existe ese tag para obtener los kpis");
			}
		} else {
			throw new PmoNotFoundException("No existe ese tag para obtener los kpis");
		}
		return kpi;
	}

	/*@PostConstruct
	public void setup() {
		kpi.save(new KpiDTO(1, "esperado", 425,17,25));
		kpi.save(new KpiDTO(2, "actual", 210,15,20));
	}*/

}
