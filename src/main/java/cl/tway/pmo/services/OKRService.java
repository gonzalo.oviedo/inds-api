package cl.tway.pmo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.tway.pmo.repositories.documents.OKRDocument;
import cl.tway.pmo.repositories.interfaces.OKRRepository;

@Service
public class OKRService {

	@Autowired
	OKRRepository okr;

	public List<OKRDocument> getAll(Integer id) {

		return okr.findAll(id);

	}

}
