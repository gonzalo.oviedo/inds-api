package cl.tway.pmo.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.jooq.lambda.Seq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.tway.pmo.business.planification.AccomplishmentBO;
import cl.tway.pmo.dtos.QualifierDTO;
import cl.tway.pmo.dtos.StackedChartDTO;
import cl.tway.pmo.dtos.planification.AccomplishmentDTO;
import cl.tway.pmo.exceptions.PmoWrongEntryException;
import cl.tway.pmo.repositories.documents.BiBarDocument;
import cl.tway.pmo.repositories.documents.Data;
import cl.tway.pmo.repositories.documents.NombresChart;
import cl.tway.pmo.repositories.documents.StageBarDocument;
import cl.tway.pmo.repositories.documents.charts.DataSeriesTypeThreeDTO;
import cl.tway.pmo.repositories.documents.planification.ProjectDocument;
import cl.tway.pmo.repositories.documents.planification.dto.SummaryPlanificationDTO;
import cl.tway.pmo.repositories.entities.h2.Actividad;
import cl.tway.pmo.repositories.entities.h2.Cumplimiento;
import cl.tway.pmo.repositories.entities.h2.Etapa;
import cl.tway.pmo.repositories.entities.h2.Responsable;
import cl.tway.pmo.repositories.entities.h2.Seguimiento;
import cl.tway.pmo.repositories.entities.mongo.DatabaseSequence;
import cl.tway.pmo.repositories.interfaces.ActividadRepository;
import cl.tway.pmo.repositories.interfaces.CumplimientoRepository;
import cl.tway.pmo.repositories.interfaces.EtapaMongoRepository;
import cl.tway.pmo.repositories.interfaces.EtapaRepository;
import cl.tway.pmo.repositories.interfaces.PlanificationRepository;
import cl.tway.pmo.repositories.interfaces.ResponsableRepository;
import cl.tway.pmo.repositories.interfaces.SeguimientoRepository;
import cl.tway.pmo.utils.Fecha;
import cl.tway.pmo.utils.Filters;
import cl.tway.pmo.utils.Util;

@Service
public class PlanificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanificationService.class);

	@Autowired
	PlanificationRepository pr;

	@Autowired
	EtapaRepository er;
	
	@Autowired
	EtapaMongoRepository erm;

	@Autowired
	ResponsableRepository rr;

	@Autowired
	ActividadRepository ar;

	@Autowired
	SeguimientoRepository sr;

	@Autowired
	CumplimientoRepository cr;
	
	@Autowired
	private SequenceGeneratorService service;

	public List<StageBarDocument> getMulti(Integer id) {
		return pr.getStages();
	}

	public BiBarDocument getBi(Integer id) {
		return pr.getBi(id);
	}

	public ProjectDocument setup(Integer projectId) {
		return pr.getSetup(projectId);
	}

	public void savePlan(String projectId, String realStartDate, String realEndDate) throws PmoWrongEntryException {

		Integer id = 0;

		try {
			id = Integer.parseInt(projectId);
		} catch (NumberFormatException nex) {
			throw new PmoWrongEntryException("El identificador del proyecto está erróneo: " + projectId);
		}

		Date realStart;
		try {
			realStart = new SimpleDateFormat("yy-MM-dd").parse(realStartDate);
		} catch (ParseException e) {
			throw new PmoWrongEntryException("La fecha de inicio real tiene un formato invalido: " + realStartDate);
		}
		Date realEnd;
		try {
			realEnd = new SimpleDateFormat("yy-MM-dd").parse(realEndDate);
		} catch (ParseException e) {
			throw new PmoWrongEntryException("La fecha de término real tiene un formato invalido: " + realEndDate);
		}

		String fStart = new SimpleDateFormat("dd-MM-yyyy").format(realStart);
		String fEnd = new SimpleDateFormat("dd-MM-yyyy").format(realEnd);

		LOGGER.debug(fStart);
		LOGGER.debug(fEnd);

	}
	
	public void saveStage(Etapa etapa) {

		Date realStart, realEnd;

		Boolean isEmptyName = etapa.getEtapa().isEmpty();

		if (isEmptyName) {
			throw new PmoWrongEntryException("Debe ingresar un nombre para la etapa");
		}

		try {
			realStart = new SimpleDateFormat("dd/MM/yyyy").parse(etapa.getInicio());
		} catch (ParseException e) {
			String message = etapa.getInicio().isEmpty() ? "No ingresó una fecha de inicio" : etapa.getInicio();
			throw new PmoWrongEntryException("La fecha de inicio de la etapa tiene un formato invalido: " + message);
		}

		try {
			realEnd = new SimpleDateFormat("dd/MM/yyyy").parse(etapa.getTermino());
		} catch (ParseException e) {
			String message = etapa.getTermino().isEmpty() ? "No ingresó una fecha de término" : etapa.getTermino();
			throw new PmoWrongEntryException(
					"La fecha de término de la etapa tiene un formato invalido: " + etapa.getTermino());
		}

		String fStart = new SimpleDateFormat("dd-MM-yyyy").format(realStart);
		String fEnd = new SimpleDateFormat("dd-MM-yyyy").format(realEnd);

		etapa.setInicio(fStart);
		etapa.setTermino(fEnd);
		
		er.save(etapa);
	}
	
	public void saveStage(cl.tway.pmo.repositories.entities.mongo.Etapa etapa) {

		Date realStart, realEnd;

		Boolean isEmptyName = etapa.getEtapa().isEmpty();

		if (isEmptyName) {
			throw new PmoWrongEntryException("Debe ingresar un nombre para la etapa");
		}

		try {
			realStart = new SimpleDateFormat("dd/MM/yyyy").parse(etapa.getInicio());
		} catch (ParseException e) {
			String message = etapa.getInicio().isEmpty() ? "No ingresó una fecha de inicio" : etapa.getInicio();
			throw new PmoWrongEntryException("La fecha de inicio de la etapa tiene un formato invalido: " + message);
		}

		try {
			realEnd = new SimpleDateFormat("dd/MM/yyyy").parse(etapa.getTermino());
		} catch (ParseException e) {
			String message = etapa.getTermino().isEmpty() ? "No ingresó una fecha de término" : etapa.getTermino();
			throw new PmoWrongEntryException(
					"La fecha de término de la etapa tiene un formato invalido: " + etapa.getTermino());
		}

		String fStart = new SimpleDateFormat("dd-MM-yyyy").format(realStart);
		String fEnd = new SimpleDateFormat("dd-MM-yyyy").format(realEnd);

		etapa.setInicio(fStart);
		etapa.setTermino(fEnd);
		
		etapa.setId(service.getSequenceNumber(etapa.SEQUENCE_NAME));

		//er.save(etapa);
		erm.save(etapa);

	}

	public void saveActivity(Actividad actividad) {

		Integer id = 0;
		id = actividad.getProyectoId();

		Date realStart, realEnd;

		Boolean isEmptyName = actividad.getNombre().isEmpty();
		Integer etapaId = actividad.getEtapaId();
		Integer responsableId = actividad.getResponsableId();

		if (etapaId == null || etapaId <= 0) {
			throw new PmoWrongEntryException("Debe ingresar una etapa para la actividad");
		}

		if (responsableId == null || responsableId <= 0) {
			throw new PmoWrongEntryException("Debe existir un responsable de la actividad");
		}

		if (isEmptyName) {
			throw new PmoWrongEntryException("Debe ingresar un nombre para la actividad");
		}

		try {
			realStart = new SimpleDateFormat("yy-MM-dd").parse(actividad.getInicio());
		} catch (ParseException e) {
			String message = actividad.getInicio().isEmpty() ? "No ingresó una fecha de inicio" : actividad.getInicio();
			throw new PmoWrongEntryException("La fecha de inicio tiene un formato invalido: " + message);
		}

		try {
			realEnd = new SimpleDateFormat("yy-MM-dd").parse(actividad.getTermino());
		} catch (ParseException e) {
			String message = actividad.getTermino().isEmpty() ? "No ingresó una fecha de término"
					: actividad.getTermino();
			throw new PmoWrongEntryException("La fecha de término tiene un formato invalido: " + message);
		}

		// String fStart = new SimpleDateFormat("dd-MM-yyyy").format(realStart);
		// String fEnd = new SimpleDateFormat("dd-MM-yyyy").format(realEnd);

		ar.save(actividad);

	}

	public List<Etapa> getStages(Integer projectId) {
		return er.findAll();
	}

	public void deleteStage(Integer stageId) {

		// FIX: Control exception by error
		er.deleteById(stageId);
	}

	public List<Actividad> getActivities() {

		List<Actividad> actividades = ar.findAll().stream().map(t -> {
			t.setInicio(Util.revertEnglishDateToChileanDate(t.getInicio()));
			t.setTermino(Util.revertEnglishDateToChileanDate(t.getTermino()));
			return t;
		}).collect(Collectors.toList());

		return actividades;
	}

	public List<Etapa> stagesInExistence() {
		List<Etapa> etapas = er.findAll().stream().filter(Filters.distinctByKey(p -> p.getEtapa()))
				.collect(Collectors.toList());

		return etapas;
	}

	public void deleteActivity(Integer id) {
		// FIX: Control exception by error
		ar.deleteById(id);
	}

	/**
	 * Primer grafico de planificación, de cabecera.
	 * 
	 * @return
	 */
	public Map<String, Object> getChartSummary() {

		Map<String, Object> output = new HashMap<String, Object>();

		List<DataSeriesTypeThreeDTO> series = new ArrayList<DataSeriesTypeThreeDTO>();

		DataSeriesTypeThreeDTO d = null;
		List<Integer> data = null;

		// Avance Real
		Integer avanceReal, avanceEsperado, atraso = 0;

		avanceReal = 14;
		avanceEsperado = 22;
		atraso = avanceEsperado - avanceReal;

		SummaryPlanificationDTO sp = new SummaryPlanificationDTO();
		sp.setReal(avanceReal);
		sp.setEsperado(avanceEsperado);
		sp.setAtraso(atraso);

		output.put("data", sp);

		data = new ArrayList<Integer>();
		data.add(avanceReal);
		data.add(0);
		data.add(0);
		d = new DataSeriesTypeThreeDTO();
		d.setName("Avance Real");
		d.setFillColor("#EB8C87");
		d.setData(data);

		series.add(d);

		data = new ArrayList<Integer>();
		data.add(0);
		data.add(avanceEsperado);
		data.add(0);
		d = new DataSeriesTypeThreeDTO();
		d.setName("Avance Esperado");
		d.setFillColor("#EB8C87");
		d.setData(data);

		series.add(d);

		data = new ArrayList<Integer>();
		data.add(0);
		data.add(0);
		data.add(atraso);

		d = new DataSeriesTypeThreeDTO();
		d.setName("Atraso");
		d.setFillColor("#EB8C87");
		d.setData(data);

		series.add(d);
		/*
		 * data = new ArrayList<Integer>(); data.add(0); data.add(934); data.add(0); d =
		 * new DataSeriesTypeThreeDTO(); d.setName("Pepe"); d.setData(data);
		 * 
		 * 
		 * series.add(d);
		 */

		output.put("series", series);

		return output;
	}

	public List<Responsable> getResponsible() {
		return rr.findAll();
	}

	public List<Actividad> activitiesByStage(Integer stageId) {
		return ar.findByEtapaId(stageId);
	}

	public void savePlanificationTracing(Seguimiento seguimiento) {

		Integer actividadId, etapaId = 0;
		Short avanceReal = 0;
		String fecha = "";

		actividadId = seguimiento.getActividadId();
		etapaId = seguimiento.getEtapaId();
		avanceReal = seguimiento.getAvanceReal();
		fecha = seguimiento.getFecha();

		Date realStart, realEnd;

		Boolean isFechaVacia = fecha.isEmpty();

		if (etapaId == 0) {
			throw new PmoWrongEntryException("Debe ingresar una etapa");
		}

		if (actividadId == 0) {
			throw new PmoWrongEntryException("Debe ingresar una actividad");
		}

		if (isFechaVacia) {
			throw new PmoWrongEntryException("Debe ingresar una fecha");
		}

		if (avanceReal == 0) {
			throw new PmoWrongEntryException("Debe ingresar un porcentage de avance");
		}

		try {
			realStart = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
		} catch (ParseException e) {
			String message = fecha.isEmpty() ? "No ingresó una fecha de inicio" : fecha;
			throw new PmoWrongEntryException("La fecha de inicio de la etapa tiene un formato invalido: " + message);
		}

		String fStart = new SimpleDateFormat("dd-MM-yyyy").format(realStart);

		seguimiento.setFecha(fStart);

		sr.save(seguimiento);
	}

	public List<AccomplishmentDTO> accomplishments(Integer projectId) {

		AccomplishmentBO bo = new AccomplishmentBO();

		List<Cumplimiento> ac = cr.findAll().stream().collect(Collectors.toList());
		List<AccomplishmentDTO> acDTO = new ArrayList<AccomplishmentDTO>();

		for (Cumplimiento c : ac) {
			QualifierDTO q = null;
			AccomplishmentDTO d = new AccomplishmentDTO();

			// Progress
			q = new QualifierDTO();
			q.setValue(c.getProgressDeviation());
			q.setQualifier(bo.getQualifier(c.getProgressDeviation()));
			d.setProgressDeviation(q);

			q = new QualifierDTO();
			q.setValue(c.getProgressPlan());
			q.setQualifier(bo.getQualifier(c.getProgressPlan()));
			d.setProgressPlan(q);

			q = new QualifierDTO();
			q.setValue(c.getProgressReal());
			q.setQualifier(bo.getQualifier(c.getProgressReal()));
			d.setProgressReal(q);

			// Init
			q = new QualifierDTO();
			q.setValue(c.getInitDeviation());
			q.setQualifier(bo.getQualifier(c.getInitDeviation()));
			d.setInitDeviation(q);

			q = new QualifierDTO();
			q.setValue(c.getInitPlan());
			q.setQualifier(bo.getQualifier(c.getInitPlan()));
			d.setInitPlan(q);

			q = new QualifierDTO();
			q.setValue(c.getInitReal());
			q.setQualifier(bo.getQualifier(c.getInitReal()));
			d.setInitReal(q);

			// End
			q = new QualifierDTO();
			q.setValue(c.getEndDeviation());
			q.setQualifier(bo.getQualifier(c.getEndDeviation()));
			d.setEndDeviation(q);

			q = new QualifierDTO();
			q.setValue(c.getEndPlan());
			q.setQualifier(bo.getQualifier(c.getEndPlan()));
			d.setEndPlan(q);

			q = new QualifierDTO();
			q.setValue(c.getEndReal());
			q.setQualifier(bo.getQualifier(c.getEndReal()));
			d.setEndReal(q);

			// Id
			d.setId(c.getId());
			d.setResponsable(c.getResponsable());

			acDTO.add(d);
		}

		return acDTO;

	}

	public List<Map<String, String>> getAccomplishments() {

		List<Map<String, String>> l = new ArrayList<Map<String, String>>();

		Map<String, String> mockServicio = null;

		mockServicio = new HashMap<String, String>();
		mockServicio.put("tipoFila", "etapa");
		mockServicio.put("nombre", "Levantamiento de Requerimiento");
		mockServicio.put("avancePlanificado", "");
		mockServicio.put("avanceReal", "");
		mockServicio.put("desviacionAvance", "");
		mockServicio.put("inicioPlanificado", "01-01-2022");
		mockServicio.put("inicioReal", "01-01-2022");
		mockServicio.put("desviacionInicio", "0");
		mockServicio.put("fechaFinPlanificado", "10-03-2022");
		mockServicio.put("fechaFinReal", "04-04-2022");
		mockServicio.put("desviacionFin", "25");
		mockServicio.put("nombreMiembro", "Responsable 1");
		l.add(mockServicio);

		mockServicio = new HashMap<String, String>();
		mockServicio.put("tipoFila", "actividad");
		mockServicio.put("nombre", "Requerimientos Contables y Documentos");
		mockServicio.put("avancePlanificado", "100");
		mockServicio.put("avanceReal", "100");
		mockServicio.put("desviacionAvance", "0");
		mockServicio.put("inicioPlanificado", "01-01-2022");
		mockServicio.put("inicioReal", "01-01-2022");
		mockServicio.put("desviacionInicio", "0");
		mockServicio.put("fechaFinPlanificado", "12-02-2022");
		mockServicio.put("fechaFinReal", "22-02-2022");
		mockServicio.put("desviacionFin", "10");
		mockServicio.put("nombreMiembro", "Responsable 3");
		l.add(mockServicio);

		mockServicio = new HashMap<String, String>();
		mockServicio.put("tipoFila", "actividad");
		mockServicio.put("nombre", "Requerimientos Financieros y Documentos");
		mockServicio.put("avancePlanificado", "178");
		mockServicio.put("avanceReal", "85");
		mockServicio.put("desviacionAvance", "-93");
		mockServicio.put("inicioPlanificado", "15-02-2022");
		mockServicio.put("inicioReal", "18-01-2022");
		mockServicio.put("desviacionInicio", "3");
		mockServicio.put("fechaFinPlanificado", "14-03-2022");
		mockServicio.put("fechaFinReal", "04-04-2022");
		mockServicio.put("desviacionFin", "21");
		mockServicio.put("nombreMiembro", "Responsable 3");
		l.add(mockServicio);

		mockServicio = new HashMap<String, String>();
		mockServicio.put("tipoFila", "actividad");
		mockServicio.put("nombre", "Perfiles de Usuario");
		mockServicio.put("avancePlanificado", "151");
		mockServicio.put("avanceReal", "60");
		mockServicio.put("desviacionAvance", "-91");
		mockServicio.put("inicioPlanificado", "04-02-2022");
		mockServicio.put("inicioReal", "03-02-2022");
		mockServicio.put("desviacionInicio", "-1");
		mockServicio.put("fechaFinPlanificado", "15-03-2022");
		mockServicio.put("fechaFinReal", "01-04-2022");
		mockServicio.put("desviacionFin", "17");
		mockServicio.put("nombreMiembro", "Responsable 5");
		l.add(mockServicio);

		mockServicio = new HashMap<String, String>();
		mockServicio.put("tipoFila", "etapa");
		mockServicio.put("nombre", "Configuración Solución");
		mockServicio.put("avancePlanificado", "");
		mockServicio.put("avanceReal", "");
		mockServicio.put("desviacionAvance", "");
		mockServicio.put("inicioPlanificado", "02-03-2022");
		mockServicio.put("inicioReal", "02-03-2022");
		mockServicio.put("desviacionInicio", "0");
		mockServicio.put("fechaFinPlanificado", "05-05-2022");
		mockServicio.put("fechaFinReal", "");
		mockServicio.put("desviacionFin", "");
		mockServicio.put("nombreMiembro", "Responsable 8");
		l.add(mockServicio);

		mockServicio = new HashMap<String, String>();
		mockServicio.put("tipoFila", "actividad");
		mockServicio.put("nombre", "Parametrizar Módulo Contable");
		mockServicio.put("avancePlanificado", "97");
		mockServicio.put("avanceReal", "5");
		mockServicio.put("desviacionAvance", "-92");
		mockServicio.put("inicioPlanificado", "02-03-2022");
		mockServicio.put("inicioReal", "02-03-2022");
		mockServicio.put("desviacionInicio", "0");
		mockServicio.put("fechaFinPlanificado", "05-04-2022");
		mockServicio.put("fechaFinReal", "04-04-2022");
		mockServicio.put("desviacionFin", "-1");
		mockServicio.put("nombreMiembro", "Responsable 13");
		l.add(mockServicio);

		mockServicio = new HashMap<String, String>();
		mockServicio.put("tipoFila", "actividad");
		mockServicio.put("nombre", "Parametrizar Módulo Financiero");
		mockServicio.put("avancePlanificado", "65");
		mockServicio.put("avanceReal", "0");
		mockServicio.put("desviacionAvance", "-65");
		mockServicio.put("inicioPlanificado", "15-03-2022");
		mockServicio.put("inicioReal", "17-03-2022");
		mockServicio.put("desviacionInicio", "2");
		mockServicio.put("fechaFinPlanificado", "15-04-2022");
		mockServicio.put("fechaFinReal", "");
		mockServicio.put("desviacionFin", "");
		mockServicio.put("nombreMiembro", "Responsable 13");
		l.add(mockServicio);

		mockServicio = new HashMap<String, String>();
		mockServicio.put("tipoFila", "actividad");
		mockServicio.put("nombre", "Parametrizar Informes");
		mockServicio.put("avancePlanificado", "6");
		mockServicio.put("avanceReal", "0");
		mockServicio.put("desviacionAvance", "-6");
		mockServicio.put("inicioPlanificado", "03-04-2022");
		mockServicio.put("inicioReal", "03-04-2022");
		mockServicio.put("desviacionInicio", "0");
		mockServicio.put("fechaFinPlanificado", "20-04-2022");
		mockServicio.put("fechaFinReal", "");
		mockServicio.put("desviacionAvance", "");
		mockServicio.put("nombreMiembro", "Responsable 8");
		l.add(mockServicio);

		mockServicio = new HashMap<String, String>();
		mockServicio.put("tipoFila", "actividad");
		mockServicio.put("nombre", "Parametrizar Perfiles");
		mockServicio.put("avancePlanificado", "0");
		mockServicio.put("avanceReal", "0");
		mockServicio.put("desviacionAvance", "0");
		mockServicio.put("inicioPlanificado", "18-04-2022");
		mockServicio.put("inicioReal", "");
		mockServicio.put("desviacionInicio", "");
		mockServicio.put("fechaFinPlanificado", "05-05-2022");
		mockServicio.put("fechaFinReal", "");
		mockServicio.put("desviacionFin", "");
		mockServicio.put("nombreMiembro", "Responsable 5");
		l.add(mockServicio);

		mockServicio = new HashMap<String, String>();
		mockServicio.put("tipoFila", "etapa");
		mockServicio.put("nombre", "Pruebas de Usuario");
		mockServicio.put("avancePlanificado", "");
		mockServicio.put("avanceReal", "");
		mockServicio.put("desviacionAvance", "");
		mockServicio.put("inicioPlanificado", "02-05-2022");
		mockServicio.put("inicioReal", "");
		mockServicio.put("desviacionInicio", "");
		mockServicio.put("fechaFinPlanificado", "");
		mockServicio.put("fechaFinReal", "12-06-2022");
		mockServicio.put("desviacionFin", "");
		mockServicio.put("nombreMiembro", "Responsable 3");
		l.add(mockServicio);

		mockServicio = new HashMap<String, String>();
		mockServicio.put("tipoFila", "actividad");
		mockServicio.put("nombre", "Prueba de Vistas");
		mockServicio.put("avancePlanificado", "0");
		mockServicio.put("avanceReal", "0");
		mockServicio.put("desviacionAvance", "0");
		mockServicio.put("inicioPlanificado", "02-05-2022");
		mockServicio.put("inicioReal", "");
		mockServicio.put("desviacionInicio", "");
		mockServicio.put("fechaFinPlanificado", "14-05-2022");
		mockServicio.put("fechaFinReal", "");
		mockServicio.put("desviacionFin", "");
		mockServicio.put("nombreMiembro", "Responsable 1");
		l.add(mockServicio);

		mockServicio = new HashMap<String, String>();
		mockServicio.put("tipoFila", "actividad");
		mockServicio.put("nombre", "Prueba de Flujos");
		mockServicio.put("avancePlanificado", "0");
		mockServicio.put("avanceReal", "0");
		mockServicio.put("desviacionAvance", "0");
		mockServicio.put("inicioPlanificado", "02-05-2022");
		mockServicio.put("inicioReal", "");
		mockServicio.put("desviacionInicio", "");
		mockServicio.put("fechaFinPlanificado", "24-05-2022");
		mockServicio.put("fechaFinReal", "");
		mockServicio.put("desviacionFin", "");
		mockServicio.put("nombreMiembro", "Responsable 1");
		l.add(mockServicio);

		mockServicio = new HashMap<String, String>();
		mockServicio.put("tipoFila", "actividad");
		mockServicio.put("nombre", "Prueba de Informes");
		mockServicio.put("avancePlanificado", "0");
		mockServicio.put("avanceReal", "0");
		mockServicio.put("desviacionAvance", "0");
		mockServicio.put("inicioPlanificado", "04-06-2022");
		mockServicio.put("inicioReal", "");
		mockServicio.put("desviacionInicio", "");
		mockServicio.put("fechaFinPlanificado", "12-06-2022");
		mockServicio.put("fechaFinReal", "");
		mockServicio.put("desviacionFin", "");
		mockServicio.put("nombreMiembro", "Responsable 13");
		l.add(mockServicio);

		return l;
	}

	public List<NombresChart> getChartStagesAndActivities() throws ParseException {

		List<NombresChart> chart = new ArrayList<NombresChart>();

		ArrayList<Long> fechas = new ArrayList<Long>();
		Data dto = null;

		// Bob
		List<Data> listaDataBob = new ArrayList<Data>();
		List<Etapa> bobData = new ArrayList<Etapa>();
		er.findAll().stream().filter(d -> d.getNombre().equals("Bob")).iterator().forEachRemaining(bobData::add);
		for (Etapa etapa : bobData) {
			fechas = new ArrayList<Long>();
			fechas.add(Fecha.getTimeStampEnglish(etapa.getInicio()));
			fechas.add(Fecha.getTimeStampEnglish(etapa.getTermino()));
			dto = new Data(etapa.getEtapa(), fechas);
			listaDataBob.add(dto);
		}
		NombresChart bob = new NombresChart("Bob", listaDataBob);

		// Joe
		List<Data> listaDataJoe = new ArrayList<Data>();
		List<Etapa> joeData = new ArrayList<Etapa>();
		er.findAll().stream().filter(d -> d.getNombre().equals("Joe")).iterator().forEachRemaining(joeData::add);
		for (Etapa etapa : joeData) {
			fechas = new ArrayList<Long>();
			fechas.add(Fecha.getTimeStampEnglish(etapa.getInicio()));
			fechas.add(Fecha.getTimeStampEnglish(etapa.getTermino()));
			dto = new Data(etapa.getEtapa(), fechas);
			listaDataJoe.add(dto);
		}
		NombresChart joe = new NombresChart("Joe", listaDataJoe);

		// Dan
		List<Data> listaDataDan = new ArrayList<Data>();
		List<Etapa> danData = new ArrayList<Etapa>();
		er.findAll().stream().filter(d -> d.getNombre().equals("Dan")).iterator().forEachRemaining(danData::add);
		for (Etapa etapa : danData) {
			fechas = new ArrayList<Long>();
			fechas.add(Fecha.getTimeStampEnglish(etapa.getInicio()));
			fechas.add(Fecha.getTimeStampEnglish(etapa.getTermino()));
			dto = new Data(etapa.getEtapa(), fechas);
			listaDataDan.add(dto);
		}
		NombresChart dan = new NombresChart("Dan", listaDataDan);

		chart.add(bob);
		chart.add(joe);
		chart.add(dan);

		return chart;
	}
}
