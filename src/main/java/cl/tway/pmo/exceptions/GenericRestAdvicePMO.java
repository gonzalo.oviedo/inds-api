package cl.tway.pmo.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GenericRestAdvicePMO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GenericRestAdvicePMO.class);

	@ExceptionHandler(PmoNotFoundException.class)
	public ResponseEntity<Void> pmoNotFoundException(Exception e) {
		LOGGER.error("PmoNotFoundException: {}",e.getMessage());
		return ResponseEntity.notFound().build();
	}
	
	@ExceptionHandler(PmoWrongEntryException.class)
	public ResponseEntity<Exception> pmoWrongEntryException(Exception e) {
		LOGGER.error("PmoWrongEntryException: {}",e.getMessage());
		return ResponseEntity.badRequest().body(e);
	}
	
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<Void> runtimeException(Exception e) {
		LOGGER.error("RuntimeException: {}",e.getMessage());
		return ResponseEntity.badRequest().build();
	}

	@ExceptionHandler(NullPointerException.class)
	public ResponseEntity<Void> nullPointerException(Exception e) {
		LOGGER.error("NullPointerException: {}",e.getMessage());
		return ResponseEntity.badRequest().build();
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Void> genericException(Exception e) {
		LOGGER.error("Exception: {}",e.getMessage());
		return ResponseEntity.badRequest().build();
	}
}
