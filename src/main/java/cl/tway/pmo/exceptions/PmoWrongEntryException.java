package cl.tway.pmo.exceptions;

public class PmoWrongEntryException extends RuntimeException {
	
	private static final long serialVersionUID = -5067185494268634676L;

	public PmoWrongEntryException(String message) {
		super(message);
	}

}
