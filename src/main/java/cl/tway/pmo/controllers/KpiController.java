package cl.tway.pmo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.tway.pmo.repositories.documents.KpiDTO;
import cl.tway.pmo.services.KpiService;

@RestController
@RequestMapping("/kpis")
public class KpiController {
	
	@Autowired
	private KpiService kpi;
	
	@CrossOrigin
	@GetMapping(path="/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	public KpiDTO getEsperado(@PathVariable(name="id") String tag) {
		
		KpiDTO kpiDTO = kpi.getKpi(tag);
		return kpiDTO;
	}	
}
