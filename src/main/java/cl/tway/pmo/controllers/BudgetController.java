package cl.tway.pmo.controllers;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.tway.pmo.repositories.documents.NombresChart;
import cl.tway.pmo.repositories.documents.budget.PieTwo;
import cl.tway.pmo.repositories.documents.charts.DataSeriesTypeOneDTO;
import cl.tway.pmo.repositories.documents.charts.DataSeriesTypeThreeDTO;
import cl.tway.pmo.repositories.documents.charts.DataSeriesTypeTwoDTO;
import cl.tway.pmo.repositories.entities.h2.Etapa;
import cl.tway.pmo.repositories.entities.h2.Presupuesto;
import cl.tway.pmo.services.BudgetService;

@RestController
@RequestMapping("budget")
public class BudgetController {
	
	@Autowired
	private BudgetService bs;
	
	@CrossOrigin
	@GetMapping(path="chart/budget", produces=MediaType.APPLICATION_JSON_VALUE)
	public Integer getSemiCirularUpdatedBudget() {
		return bs.getSemiCirularUpdatedBudget();
	}
	
	@CrossOrigin
	@GetMapping(path="/all", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Presupuesto> getAll() {
		
		return bs.getAll();
	}
	
	@CrossOrigin
	@GetMapping(path="/getInitialAndBalance", produces=MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Integer> getInitialAndBalance() {
		
		return bs.getInitialAndBalance();
	}
	
	@CrossOrigin
	@GetMapping(path="/getTypes", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<?> getTypes() {
		
		return bs.getTypes();
	}
	
	@CrossOrigin
	@GetMapping(path="/getConcepts", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<?> getConcepts() {
		
		return bs.getConcepts();
	}
	
	@CrossOrigin
	@PostMapping(path="/saveExpense", produces=MediaType.APPLICATION_JSON_VALUE)
	public void saveExpense(@RequestBody Map<String, String> expenses) {
		  
		bs.saveExpense(expenses);
	}
	
	@CrossOrigin
	@GetMapping(path="/getPieTwo", produces=MediaType.APPLICATION_JSON_VALUE)
	public  PieTwo getPieTwo() {
		  
		return bs.getPieTwo();
	}
	
	@CrossOrigin
	@GetMapping(path="/timeLineChart", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<NombresChart> timeLineChart() throws ParseException {
		return bs.timeLineChart();
	}
	
	@CrossOrigin
	@GetMapping(path="/stages", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Etapa> stages() throws ParseException {
		return bs.stages();
	}
	
	@CrossOrigin
	@GetMapping(path="/stagesInExistence", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<String> stagesInExistence() throws ParseException {
		return bs.stagesInExistence();
	}
	
	/**
	 * Chart
	 * Seguimiento por periodo
	 * @return
	 */
	@CrossOrigin
	@GetMapping(path="/chartPeriodTrack", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<DataSeriesTypeOneDTO> chartPeriodTrack() {
		  
		return bs.chartPeriodTrack();
	}
	
	/**
	 * Chart
	 * Seguimiento por centro de costo
	 * @return
	 */
	@CrossOrigin
	@GetMapping(path="/chartTrackByCostCenter", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<DataSeriesTypeTwoDTO> chartTrackByCostCenter() {
		  
		return bs.chartTrackByCostCenter();
	}
	
	/**
	 * Chart
	 * Cabecera, resumen
	 * @return
	 */
	@CrossOrigin
	@GetMapping(path="/chartSummaryBudget", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<DataSeriesTypeThreeDTO> chartSummaryBudget() {
		  
		return bs.chartSummaryBudget();
	}
	
	/**
	 * Chart
	 * Centro costo
	 * @return
	 */
	@CrossOrigin
	@GetMapping(path="/chartCostCenter", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<DataSeriesTypeThreeDTO> chartCostCenter() {
		  
		return bs.chartCostCenter();
	}

}
