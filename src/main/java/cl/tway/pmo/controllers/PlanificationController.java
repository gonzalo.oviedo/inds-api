package cl.tway.pmo.controllers;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.tway.pmo.dtos.StackedChartDTO;
import cl.tway.pmo.dtos.planification.AccomplishmentDTO;
import cl.tway.pmo.repositories.documents.BiBarDocument;
import cl.tway.pmo.repositories.documents.NombresChart;
import cl.tway.pmo.repositories.documents.StageBarDocument;
import cl.tway.pmo.repositories.documents.charts.DataSeriesTypeThreeDTO;
import cl.tway.pmo.repositories.documents.planification.ProjectDocument;
import cl.tway.pmo.repositories.entities.h2.Actividad;
import cl.tway.pmo.repositories.entities.h2.Etapa;
import cl.tway.pmo.repositories.entities.h2.Responsable;
import cl.tway.pmo.repositories.entities.h2.Seguimiento;
import cl.tway.pmo.services.PlanificationService;

@RestController
@RequestMapping("plan")
public class PlanificationController {

	@Autowired
	private PlanificationService ps;

	@CrossOrigin
	@GetMapping(path = "chart/bi/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public BiBarDocument getBi(@PathVariable(name = "id") Integer id) {
		return ps.getBi(id);
	}

	@CrossOrigin
	@GetMapping(path = "chart/multi/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<StageBarDocument> getMulti(@PathVariable(name = "id") Integer id) {
		return ps.getMulti(id);
	}

	@CrossOrigin
	@PostMapping(path = "setup/{projectId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ProjectDocument setup(@PathVariable(name = "projectId") Integer projectId) {
		return ps.setup(projectId);
	}

	@CrossOrigin
	@RequestMapping("/savePlan")
	public ResponseEntity<Void> savePlan(@RequestParam String projectId, @RequestParam String realStartDate,
			@RequestParam String realEndDate) {

		ps.savePlan(projectId, realStartDate, realEndDate);

		return ResponseEntity.ok().build();
	}

	@CrossOrigin
	@RequestMapping("/saveActivity")
	public ResponseEntity<Void> saveActivity(@RequestBody Actividad actividad) {

		ps.saveActivity(actividad);

		return ResponseEntity.ok().build();
	}

	@CrossOrigin
	@RequestMapping(value = "/saveStage", method = RequestMethod.POST)
	public ResponseEntity<Void> saveStage(@RequestBody cl.tway.pmo.repositories.entities.mongo.Etapa etapa) throws ParseException {

		ps.saveStage(etapa);

		return ResponseEntity.ok().build();
	}
	
	@CrossOrigin
	@RequestMapping(value = "/savePlanificationTracing", method = RequestMethod.POST)
	public ResponseEntity<Void> savePlanificationTracing(@RequestBody Seguimiento seguimiento) throws ParseException {

		ps.savePlanificationTracing(seguimiento);

		return ResponseEntity.ok().build();
	}

	@CrossOrigin
	@RequestMapping(path = "/getStages/{projectId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Etapa> getStages(@PathVariable(name = "projectId") Integer projectId) {
		return ps.getStages(projectId);
	}
	
	@CrossOrigin
	@RequestMapping(path = "/activitiesByStage/{stageId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Actividad> activitiesByStage(@PathVariable(name = "stageId") Integer stageId) {
		return ps.activitiesByStage(stageId);
	}
	
	@CrossOrigin
	@RequestMapping(path = "/getActivities", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Actividad> getActivities() {
		return ps.getActivities();
	}

	@CrossOrigin
	@DeleteMapping(value = "/deleteStage/{stageId}")
	public ResponseEntity<Void> deleteStage(@PathVariable(name = "stageId") Integer stageId) {

		ps.deleteStage(stageId);

		return ResponseEntity.ok().build();
	}
	
	@CrossOrigin
	@DeleteMapping(value = "/deleteActivity/{id}")
	public ResponseEntity<Void> deleteActivity(@PathVariable(name = "id") Integer id) {

		ps.deleteActivity(id);

		return ResponseEntity.ok().build();
	}
	
	@CrossOrigin
	@GetMapping(path="/stagesInExistence", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Etapa> stagesInExistence() throws ParseException {
		return ps.stagesInExistence();
	}

	@CrossOrigin
	@GetMapping(path="/getChartSummary", produces=MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object>  getChartSummary() throws ParseException {
		return ps.getChartSummary();
	}
	
	@CrossOrigin
	@GetMapping(path="/getResponsible", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Responsable> getResponsible() throws ParseException {
		return ps.getResponsible();
	}
	
	@CrossOrigin
	@GetMapping(path = "/accomplishments/{projectId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AccomplishmentDTO> accomplishments(@PathVariable(name = "projectId") Integer projectId) {
		return ps.accomplishments(projectId);
	}
	
	@CrossOrigin
	@GetMapping(path="/getAccomplishments", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Map<String, String>>  getAccomplishments() throws ParseException {
		return ps.getAccomplishments();
	}
	
	@CrossOrigin
	@GetMapping(path="/getChartStagesAndActivities", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<NombresChart> getChartStagesAndActivities() throws ParseException {
		return ps.getChartStagesAndActivities();
	}
	
}
