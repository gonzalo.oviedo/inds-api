package cl.tway.pmo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.tway.pmo.repositories.documents.KpiDTO;
import cl.tway.pmo.repositories.documents.OKRDocument;
import cl.tway.pmo.services.KpiService;
import cl.tway.pmo.services.OKRService;

@RestController
@RequestMapping("okr")
public class OKRController {
	
	@Autowired
	private OKRService okr;
	
	@CrossOrigin
	@GetMapping(path="chart/cake/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<OKRDocument> getAll(@PathVariable(name="id") Integer id) {
		
		return okr.getAll(id);
	}
}
