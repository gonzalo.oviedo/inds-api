package cl.tway.pmo.controllers;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.tway.pmo.dtos.StackedChartDTO;
import cl.tway.pmo.dtos.planification.AccomplishmentDTO;
import cl.tway.pmo.repositories.documents.BiBarDocument;
import cl.tway.pmo.repositories.documents.StageBarDocument;
import cl.tway.pmo.repositories.documents.charts.DataSeriesTypeThreeDTO;
import cl.tway.pmo.repositories.documents.planification.ProjectDocument;
import cl.tway.pmo.repositories.entities.h2.Actividad;
import cl.tway.pmo.repositories.entities.h2.Etapa;
import cl.tway.pmo.repositories.entities.h2.Responsable;
import cl.tway.pmo.repositories.entities.h2.Seguimiento;
import cl.tway.pmo.services.ObjetivesService;
import cl.tway.pmo.services.PlanificationService;

@RestController
@RequestMapping("objetives")
public class ObjetivesController {

	@Autowired
	private ObjetivesService os;
	
	@CrossOrigin
	@GetMapping(path="/getSummaryObjetives", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Map<String, Object>>  getSummaryObjetives() throws ParseException {
		return os.getChartSummary();
	}
	
	@CrossOrigin
	@GetMapping(path="/getCircleData", produces=MediaType.APPLICATION_JSON_VALUE)
	public Map<String, List<Integer>>  getCircleData() throws ParseException {
		return os.getCircleData();
	}
	
	
}
