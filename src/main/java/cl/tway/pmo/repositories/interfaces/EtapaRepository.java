package cl.tway.pmo.repositories.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.tway.pmo.repositories.entities.h2.Etapa;

@Repository
public interface EtapaRepository extends JpaRepository<Etapa, Integer> {
	
	List<Etapa> findAll();
}
