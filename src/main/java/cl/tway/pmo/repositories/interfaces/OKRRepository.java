package cl.tway.pmo.repositories.interfaces;

import java.util.List;

import org.springframework.stereotype.Repository;

import cl.tway.pmo.repositories.documents.OKRDocument;

@Repository
public interface OKRRepository {
	
	List<OKRDocument> findAll(Integer id);
	public long count();

}
