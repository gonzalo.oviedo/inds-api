package cl.tway.pmo.repositories.interfaces;

import java.util.List;


import cl.tway.pmo.repositories.documents.KpiDTO;

public interface KpiRepository {
	
	
	List<KpiDTO> findAll();
	
	public long count();

}
