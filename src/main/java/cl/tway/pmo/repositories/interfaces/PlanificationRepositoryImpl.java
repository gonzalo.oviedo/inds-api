package cl.tway.pmo.repositories.interfaces;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import org.springframework.stereotype.Repository;

import cl.tway.pmo.repositories.documents.BiBarDocument;
import cl.tway.pmo.repositories.documents.StageBarDocument;
import cl.tway.pmo.repositories.documents.planification.ProjectDocument;

@Repository
public class PlanificationRepositoryImpl implements PlanificationRepository {

	@Override
	public List<StageBarDocument> getStages() {
		
		
		StageBarDocument analysis = new StageBarDocument(1, "Analysis", 100, new Date(2021,8,20), new Date(2021,8,30));
		StageBarDocument design = new StageBarDocument(1, "Desgin", 100, new Date(2021,9,20), new Date(2021,9,30));
		StageBarDocument coding = new StageBarDocument(1, "Coding", 100, new Date(2021,10,20), new Date(2021,10,30));
		StageBarDocument testing = new StageBarDocument(1, "Testing", 100, new Date(2021,11,20), new Date(2021,11,30));
		StageBarDocument deployment = new StageBarDocument(1, "Deployment", 100, new Date(2021,12,20), new Date(2021,12,30));
		
		
		List<StageBarDocument> list = new ArrayList<StageBarDocument>();
		
		list.add(analysis);
		list.add(design);
		list.add(coding);
		list.add(testing);
		list.add(deployment);
		
		return list;
	}

	@Override
	public BiBarDocument getBi(Integer id) {
		BiBarDocument bar = new BiBarDocument(1, 60, 40);
		return bar;
	}

	@Override
	public ProjectDocument getSetup(Integer projectId) {
	
		String s = "2022-01-05";
		LocalDate startDate = LocalDate.parse(s);
		
		String e = "2022-10-05";
		LocalDate endDate = LocalDate.parse(e);
		
		// Human readable date
		
		String humanStartDate = startDate.format(DateTimeFormatter
			    .ofLocalizedDate(FormatStyle.LONG));
		String humanEndDate = endDate.format(DateTimeFormatter
			    .ofLocalizedDate(FormatStyle.LONG));
		
		//return new ProjectDocument("1", "TE_3120", "Curso Spring Fundamentals", String.valueOf(startDate.atStartOfDay()), String.valueOf(endDate.atStartOfDay()));
		return new ProjectDocument("1", "TE_3120", "Curso Spring Fundamentals", s, e, humanStartDate, humanEndDate);
	}
	
	

}
