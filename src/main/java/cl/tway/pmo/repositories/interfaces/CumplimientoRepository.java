package cl.tway.pmo.repositories.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.tway.pmo.repositories.entities.h2.Cumplimiento;

@Repository
public interface CumplimientoRepository extends JpaRepository<Cumplimiento, Integer> {
	
	List<Cumplimiento> findAll();
}
