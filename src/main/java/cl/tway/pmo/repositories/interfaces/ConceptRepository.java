package cl.tway.pmo.repositories.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.tway.pmo.repositories.entities.h2.Concepto;

@Repository
public interface ConceptRepository extends JpaRepository<Concepto, Integer> {
	
}
