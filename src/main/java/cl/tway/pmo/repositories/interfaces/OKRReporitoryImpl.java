package cl.tway.pmo.repositories.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import cl.tway.pmo.repositories.documents.OKRDocument;

@Component
public class OKRReporitoryImpl implements OKRRepository {
	
	private List<OKRDocument> okrs;

	@Override
	public List<OKRDocument> findAll(Integer id) {
		OKRDocument one = new OKRDocument(1, "KR1", 170);
		OKRDocument two = new OKRDocument(1, "KR2", 50);
		OKRDocument three = new OKRDocument(1, "KR3", 2);
		
		okrs = new ArrayList<OKRDocument>();
		
		okrs.add(one);
		okrs.add(two);
		okrs.add(three);
		
		return okrs;
		
	}

	@Override
	public long count() {
		return okrs.size();
	}

}
