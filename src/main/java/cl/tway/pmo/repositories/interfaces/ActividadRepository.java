package cl.tway.pmo.repositories.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.tway.pmo.repositories.entities.h2.Actividad;

@Repository
public interface ActividadRepository extends JpaRepository<Actividad, Integer> {
	
	List<Actividad> findAll();

	List<Actividad> findByEtapaId(Integer etapaId);
}
