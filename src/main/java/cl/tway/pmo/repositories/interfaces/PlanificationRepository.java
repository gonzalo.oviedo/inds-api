package cl.tway.pmo.repositories.interfaces;

import java.util.List;

import org.springframework.stereotype.Repository;

import cl.tway.pmo.repositories.documents.BiBarDocument;
import cl.tway.pmo.repositories.documents.StageBarDocument;
import cl.tway.pmo.repositories.documents.planification.ProjectDocument;

@Repository
public interface PlanificationRepository {
	
	List<StageBarDocument> getStages();

	BiBarDocument getBi(Integer id);

	ProjectDocument getSetup(Integer projectId);

}
