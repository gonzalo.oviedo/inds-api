package cl.tway.pmo.repositories.interfaces;

import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.stereotype.Repository;

import cl.tway.pmo.repositories.entities.mongo.Etapa;

@Repository
public interface EtapaMongoRepository extends MongoRepository<Etapa, Integer> {

}
