package cl.tway.pmo.repositories.entities.h2;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Seguimiento")
public class Seguimiento {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer etapaId;
	private Integer actividadId;
	private String fecha;
	private Short avanceReal;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEtapaId() {
		return etapaId;
	}

	public void setEtapaId(Integer etapaId) {
		this.etapaId = etapaId;
	}

	public Integer getActividadId() {
		return actividadId;
	}

	public void setActividadId(Integer actividadId) {
		this.actividadId = actividadId;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Short getAvanceReal() {
		return avanceReal;
	}

	public void setAvanceReal(Short avanceReal) {
		this.avanceReal = avanceReal;
	}
}
