package cl.tway.pmo.repositories.entities.h2;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Cumplimiento")
public class Cumplimiento {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer progressPlan;
    private Integer progressReal;
    private Integer progressDeviation;
    private Integer initPlan;
    private Integer initReal;
    private Integer initDeviation;
    private Integer endPlan;
    private Integer endReal;
    private Integer endDeviation;
    private String responsable;
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getProgressPlan() {
		return progressPlan;
	}
	public void setProgressPlan(Integer progressPlan) {
		this.progressPlan = progressPlan;
	}
	public Integer getProgressReal() {
		return progressReal;
	}
	public void setProgressReal(Integer progressReal) {
		this.progressReal = progressReal;
	}
	public Integer getProgressDeviation() {
		return progressDeviation;
	}
	public void setProgressDeviation(Integer progressDeviation) {
		this.progressDeviation = progressDeviation;
	}
	public Integer getInitPlan() {
		return initPlan;
	}
	public void setInitPlan(Integer initPlan) {
		this.initPlan = initPlan;
	}
	public Integer getInitReal() {
		return initReal;
	}
	public void setInitReal(Integer initReal) {
		this.initReal = initReal;
	}
	public Integer getInitDeviation() {
		return initDeviation;
	}
	public void setInitDeviation(Integer initDeviation) {
		this.initDeviation = initDeviation;
	}
	public Integer getEndPlan() {
		return endPlan;
	}
	public void setEndPlan(Integer endPlan) {
		this.endPlan = endPlan;
	}
	public Integer getEndReal() {
		return endReal;
	}
	public void setEndReal(Integer endReal) {
		this.endReal = endReal;
	}
	public Integer getEndDeviation() {
		return endDeviation;
	}
	public void setEndDeviation(Integer endDeviation) {
		this.endDeviation = endDeviation;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
    
    

}
