package cl.tway.pmo.repositories.entities.mongo;

import javax.persistence.Id;
import javax.persistence.Transient;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Etapa")
public class Etapa {
	
	@Id
	private Integer id;
	
	@Transient
	public static final String SEQUENCE_NAME = "user_sequence";
	
	private Integer orden;
	private String nombre;
	private String etapa;
	private String inicio;
	private String termino;
	private Integer proyectoId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEtapa() {
		return etapa;
	}
	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}
	public String getInicio() {
		return  inicio;
	}
	public void setInicio(String inicio) {
		this.inicio = inicio;
	}
	public String getTermino() {
		return termino;
	}
	public void setTermino(String termino) {
		this.termino = termino;
	}
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	public Integer getProyectoId() {
		return proyectoId;
	}
	public void setProyectoId(Integer proyectoId) {
		this.proyectoId = proyectoId;
	}
}
