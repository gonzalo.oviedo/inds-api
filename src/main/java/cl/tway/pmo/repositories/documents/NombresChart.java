package cl.tway.pmo.repositories.documents;

import java.util.List;

public class NombresChart {
	
	private String name;
	private List<Data> data;
	
	public NombresChart(String name, List<Data> data) {
		this.name = name;
		this.data = data;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Data> getData() {
		return data;
	}

	public void setData(List<Data> data) {
		this.data = data;
	}

	
	

}
