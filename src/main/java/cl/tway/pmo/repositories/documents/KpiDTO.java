package cl.tway.pmo.repositories.documents;


public class KpiDTO {


	private Integer id;
	private String tag;
	private Integer grow;
	private Integer satisfaction;
	private Integer roi;
	
	public KpiDTO(Integer id, String tag, Integer grow, Integer satisfaction, Integer roi) {
		super();
		this.id = id;
		this.tag = tag;
		this.grow = grow;
		this.satisfaction = satisfaction;
		this.roi = roi;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public Integer getGrow() {
		return grow;
	}
	public void setGrow(Integer grow) {
		this.grow = grow;
	}
	public Integer getSatisfaction() {
		return satisfaction;
	}
	public void setSatisfaction(Integer satisfaction) {
		this.satisfaction = satisfaction;
	}
	public Integer getRoi() {
		return roi;
	}
	public void setRoi(Integer roi) {
		this.roi = roi;
	}
}
