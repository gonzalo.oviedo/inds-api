package cl.tway.pmo.repositories.documents;

import java.io.Serializable;
import java.util.Date;


public class StageBarDocument implements Serializable {
	
	private static final long serialVersionUID = -3980245567400178367L;
	private Integer id;
	private String tag;
	private Integer value;
	private Date startDate;
	private Date endDate;
	
	public StageBarDocument(Integer id, String tag, Integer value, Date startDate, Date endDate) {
		super();
		this.id = id;
		this.tag = tag;
		this.value = value;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	
}

