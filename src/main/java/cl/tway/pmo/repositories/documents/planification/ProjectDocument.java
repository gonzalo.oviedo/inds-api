package cl.tway.pmo.repositories.documents.planification;

import java.io.Serializable;

public class ProjectDocument implements Serializable {

	private static final long serialVersionUID = 4336236840695398195L;
	private String id;
	private String code;
	private String name;
	private String startDateProposal;
	private String endDateProposal;
	private String humanStartDateProposal;
	private String humanEndDateProposal;
	
	public ProjectDocument(String id, String code, String name, String startDateProposal, String endDateProposal, String humanStartDateProposal, String humanEndDateProposal) {
		this.id = id;
		this.code = code;
		this.name = name;
		this.startDateProposal = startDateProposal;
		this.endDateProposal = endDateProposal;
		this.humanStartDateProposal = humanStartDateProposal;
		this.humanEndDateProposal = humanEndDateProposal;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getStartDateProposal() {
		return startDateProposal;
	}

	public void setStartDateProposal(String startDateProposal) {
		this.startDateProposal = startDateProposal;
	}

	public String getEndDateProposal() {
		return endDateProposal;
	}

	public void setEndDateProposal(String endDateProposal) {
		this.endDateProposal = endDateProposal;
	}

	public String getHumanStartDateProposal() {
		return humanStartDateProposal;
	}

	public void setHumanStartDateProposal(String humanStartDateProposal) {
		this.humanStartDateProposal = humanStartDateProposal;
	}

	public String getHumanEndDateProposal() {
		return humanEndDateProposal;
	}

	public void setHumanEndDateProposal(String humanEndDateProposal) {
		this.humanEndDateProposal = humanEndDateProposal;
	}
}
