package cl.tway.pmo.repositories.documents;

import java.io.Serializable;

public class BiBarDocument implements Serializable {
	
	private static final long serialVersionUID = 3708435691513915500L;
	private Integer id;
	private Integer valueGreen;
	private Integer valueRed;
	
	public BiBarDocument(Integer id, Integer valueGreen, Integer valueRed) {
		super();
		this.id = id;
		this.valueGreen = valueGreen;
		this.valueRed = valueRed;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getValueGreen() {
		return valueGreen;
	}
	public void setValueGreen(Integer valueGreen) {
		this.valueGreen = valueGreen;
	}
	public Integer getValueRed() {
		return valueRed;
	}
	public void setValueRed(Integer valueRed) {
		this.valueRed = valueRed;
	}
	
	
	
}
