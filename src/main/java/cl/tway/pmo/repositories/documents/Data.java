package cl.tway.pmo.repositories.documents;

import java.util.List;

public class Data {
	
	private String x;
	private List<Long> y;
	
	public Data(String etapa, List<Long> fechas) {
		this.x = etapa;
		this.y = fechas;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public List<Long> getY() {
		return y;
	}

	public void setY(List<Long> y) {
		this.y = y;
	}
	
}
