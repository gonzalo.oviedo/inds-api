package cl.tway.pmo.repositories.documents.budget;

import java.io.Serializable;

public class SemiCircular implements Serializable {
	
	private static final long serialVersionUID = -3980245567400178367L;
	private Integer id;
	private Integer value;
	
	public SemiCircular(Integer id, Integer value) {
		super();
		this.id = id;
		this.value = value;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
}

