package cl.tway.pmo.repositories.documents.charts.helpers;



public class Goal {
	public String name;
	public Integer value;
	public Short strokeWidth;
	public Short strokeHeight;
	public String strokeLineCap;
	public String strokeColor;
	
	public Goal(String name, Integer value, Short strokeWidth, Short strokeHeight, String strokeColor) {
		this.name = name;
		this.value = value;
		this.strokeWidth = strokeWidth;
		this.strokeHeight = strokeHeight;
		this.strokeColor = strokeColor;
		this.strokeLineCap = "";
	}
	
	public Goal(String name, Integer value, Short strokeWidth, Short strokeHeight, String strokeLineCap, String strokeColor) {
		this.name = name;
		this.value = value;
		this.strokeWidth = strokeWidth;
		this.strokeHeight = strokeHeight;
		this.strokeLineCap = strokeLineCap;
		this.strokeColor = strokeColor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Short getStrokeWidth() {
		return strokeWidth;
	}

	public void setStrokeWidth(Short strokeWidth) {
		this.strokeWidth = strokeWidth;
	}


	public Short getStrokeHeight() {
		return strokeHeight;
	}

	public void setStrokeHeight(Short strokeHeight) {
		this.strokeHeight = strokeHeight;
	}

	public String getStrokeColor() {
		return strokeColor;
	}

	public void setStrokeColor(String strokeColor) {
		this.strokeColor = strokeColor;
	}

	public String getStrokeLineCap() {
		return strokeLineCap;
	}

	public void setStrokeLineCap(String strokeLineCap) {
		this.strokeLineCap = strokeLineCap;
	}
	
	
}
