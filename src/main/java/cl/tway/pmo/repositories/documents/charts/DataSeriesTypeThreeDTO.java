package cl.tway.pmo.repositories.documents.charts;

import java.util.List;

public class DataSeriesTypeThreeDTO {
	
	private String name;
	private List<Integer> data;
	private String fillColor;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Integer> getData() {
		return data;
	}
	public void setData(List<Integer> data) {
		this.data = data;
	}
	public String getFillColor() {
		return fillColor;
	}
	public void setFillColor(String fillColor) {
		this.fillColor = fillColor;
	}
	
}
