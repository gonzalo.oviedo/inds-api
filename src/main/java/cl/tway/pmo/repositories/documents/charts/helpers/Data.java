package cl.tway.pmo.repositories.documents.charts.helpers;

import java.util.ArrayList;


public class Data {
	
	public String x;
	public Integer y;
	public ArrayList<Goal> goals;
	
	public Data(String x, Integer y, ArrayList<Goal> goals) {
		this.x = x;
		this.y = y;
		this.goals = goals;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public ArrayList<Goal> getGoals() {
		return goals;
	}

	public void setGoals(ArrayList<Goal> goals) {
		this.goals = goals;
	}
	
	
	
}