package cl.tway.pmo.repositories.documents.charts;

import java.util.ArrayList;

import cl.tway.pmo.repositories.documents.charts.helpers.Data;

public class DataSeriesTypeTwoDTO {


	private String name;
	private ArrayList<Data> data;
	
	public DataSeriesTypeTwoDTO(String name, ArrayList<Data> data) {
		this.name = name;
		this.data = data;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Data> getData() {
		return data;
	}

	public void setData(ArrayList<Data> data) {
		this.data = data;
	}
	
	
}
