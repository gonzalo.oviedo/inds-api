package cl.tway.pmo.repositories.documents;

import java.io.Serializable;

public class OKRDocument implements Serializable {
	
	private static final long serialVersionUID = -3817518444117419269L;
	private Integer id;
	private String tag;
	private Integer value;
	
	
	
	public OKRDocument(Integer id, String tag, Integer value) {
		super();
		this.id = id;
		this.tag = tag;
		this.value = value;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	
	

}
