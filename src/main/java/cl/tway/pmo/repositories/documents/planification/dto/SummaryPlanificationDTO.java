package cl.tway.pmo.repositories.documents.planification.dto;

public class SummaryPlanificationDTO {
	
	private Integer real;
	private Integer esperado;
	private Integer atraso;
	public Integer getReal() {
		return real;
	}
	public void setReal(Integer real) {
		this.real = real;
	}
	public Integer getEsperado() {
		return esperado;
	}
	public void setEsperado(Integer esperado) {
		this.esperado = esperado;
	}
	public Integer getAtraso() {
		return atraso;
	}
	public void setAtraso(Integer atraso) {
		this.atraso = atraso;
	}
	
	

}
