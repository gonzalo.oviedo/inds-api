package cl.tway.pmo.dtos;

import java.util.ArrayList;

import cl.tway.pmo.repositories.documents.charts.helpers.Data;
import cl.tway.pmo.repositories.documents.charts.helpers.Goal;

public class StackedChartDTO {


	private String name;
	private ArrayList<Data> data;
	
	//public DataSeriesTypeTwoDTO(String name, String xAxis, Integer yAxis, String goalName, Integer goalValue, Short strokeWidth, Short strokeDashArray, String strokeColor) {
	public StackedChartDTO() {
		this.name = "Prueba";

		ArrayList<Goal> goals = new ArrayList<Goal>();
		Goal goal = new Goal("Esperado", 80, (short)2, (short)2, "#775DD0");
		goals.add(goal);
		
		ArrayList<Data> lotData = new ArrayList<Data>();
		Data data = new Data("Meta", 50, goals);
		
		lotData.add(data);
		
		this.setData(lotData);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Data> getData() {
		return data;
	}

	public void setData(ArrayList<Data> data) {
		this.data = data;
	}
	
	
}
