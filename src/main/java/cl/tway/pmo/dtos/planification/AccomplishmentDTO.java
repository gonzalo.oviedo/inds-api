package cl.tway.pmo.dtos.planification;

import cl.tway.pmo.dtos.QualifierDTO;

public class AccomplishmentDTO {

	private Integer id;
	private QualifierDTO progressPlan;
	private QualifierDTO progressReal;
	private QualifierDTO progressDeviation;
	private QualifierDTO initPlan;
	private QualifierDTO initReal;
	private QualifierDTO initDeviation;
	private QualifierDTO endPlan;
	private QualifierDTO endReal;
	private QualifierDTO endDeviation;
	private String responsable;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public QualifierDTO getProgressPlan() {
		return progressPlan;
	}

	public void setProgressPlan(QualifierDTO progressPlan) {
		this.progressPlan = progressPlan;
	}

	public QualifierDTO getProgressReal() {
		return progressReal;
	}

	public void setProgressReal(QualifierDTO progressReal) {
		this.progressReal = progressReal;
	}

	public QualifierDTO getProgressDeviation() {
		return progressDeviation;
	}

	public void setProgressDeviation(QualifierDTO progressDeviation) {
		this.progressDeviation = progressDeviation;
	}

	public QualifierDTO getInitPlan() {
		return initPlan;
	}

	public void setInitPlan(QualifierDTO initPlan) {
		this.initPlan = initPlan;
	}

	public QualifierDTO getInitReal() {
		return initReal;
	}

	public void setInitReal(QualifierDTO initReal) {
		this.initReal = initReal;
	}

	public QualifierDTO getInitDeviation() {
		return initDeviation;
	}

	public void setInitDeviation(QualifierDTO initDeviation) {
		this.initDeviation = initDeviation;
	}

	public QualifierDTO getEndPlan() {
		return endPlan;
	}

	public void setEndPlan(QualifierDTO endPlan) {
		this.endPlan = endPlan;
	}

	public QualifierDTO getEndReal() {
		return endReal;
	}

	public void setEndReal(QualifierDTO endReal) {
		this.endReal = endReal;
	}

	public QualifierDTO getEndDeviation() {
		return endDeviation;
	}

	public void setEndDeviation(QualifierDTO endDeviation) {
		this.endDeviation = endDeviation;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

}
