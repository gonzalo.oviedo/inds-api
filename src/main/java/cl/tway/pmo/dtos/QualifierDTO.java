package cl.tway.pmo.dtos;

public class QualifierDTO {
	
	private Integer value;
	private Integer qualifier;
	
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public Integer getQualifier() {
		return qualifier;
	}
	public void setQualifier(Integer qualifier) {
		this.qualifier = qualifier;
	}
}
